FROM docker.io/library/python:3.11

RUN pip install fastapi
RUN pip install union
RUN pip install "uvicorn[standard]"
RUN pip install pustil

COPY main.py .
COPY tools.py .

EXPOSE 8000
CMD["uvicorn","mian:app","--host","0.0.0.0","--port","8000"]
