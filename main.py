from typing import Union
from fastapi import FastAPI
import tools

app = FastAPI()

@app.get("/cpu_times")
def read_root():
    return tools.get_cpu_times()


