import psutil

def get_cpu_times():
    cpu_times=psutil.cpu_times()
    cpu_times_info={
        "user":cpu_times.user,
        "system":cpu_times.system,
        "idle":cpu_times.idle,
        "interrupt":cpu_times.interrupt,
        "dpc":cpu_times.dpc,
    }
    return cpu_times_info